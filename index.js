const sharp = require('sharp');
const fs = require('fs');
const spawnSync = require('child_process').spawnSync;

sharp('sample.jpg')
  .resize({ width: 1000 })
  .toFile('sample.resized.jpg');

/*
-y = Overwrite output files without asking
-i = Specify input path
-vf = Create the filtergraph specified by filtergraph and use it to filter the stream. Example: -vf=hqdn3d=1.5:1.5:6:6
scale = Resize input video. Option is part of -vf. Example: -vf "scale='min(iw,trunc(iw*min(1280/iw,720/ih)*0.5)*2)':'min(ih,trunc(ih*min(1280/iw,720/ih)*0.5)*2)'"
unsharp = Sharpen or blur the input video. Option is part of -vf. Example: unsharp=3:3:1.0:3:3:0.0
-threads = Number of threads used. Example: -threads=0 where 0 means optimal
-b:a = Conversion bitrate for audio. Example: -b:a 96k 
-ac = Set the number of audio channels. Example: -ac2 for 2 channels 
-ar = Set the audio sampling frequency. Example: -ar=44100 
-vcodec = Set the video codec. Example: -vcodec=libx264
-crf = Constant Rate Factor. Use this mode if you want to keep the best quality and don't care about the file size. Example: -crf=23 where 23 is the default. Lower number has better quality
-metadata = Set a metadata key/value pair. Example: -metadata:s:v:0 rotate=0
-movflags=faststart = Add this option if your videos are viewed in a web browser. This will move some information to the beginning of your file and allow the video to begin playing before it is completely downloaded by the viewer
-pix_fmt = Set pixel format. Example: -pix_fmt=yuv420p
*/

var cmd = 'ffmpeg';

var media = fs.readdirSync('media/');
var filteredMedia = media.filter(file => file.endsWith('.mp4'));
for (const file of filteredMedia) 
{
  var args = [
    '-y', 
    '-i', `media/${file}`,
    '-vf', `hqdn3d=1.5:1.5:6:6,scale='min(iw,trunc(iw*min(1280/iw,720/ih)*0.5)*2)':'min(ih,trunc(ih*min(1280/iw,720/ih)*0.5)*2)',unsharp=3:3:1.0:3:3:0.0`, 
    '-threads', '0', 
    '-b:a', '96k', 
    '-ac', '2',
    '-ar', '44100',
    '-vcodec', 'libx264', 
    '-crf', '23', 
    '-metadata:s:v:0', 'rotate=0',
    '-movflags', 'faststart',
    '-pix_fmt', 'yuv420p', 
    `media/output/${file}`
  ];

  var proc = spawnSync(cmd, args, { stdio: 'inherit' });
  console.log(`Finished converting ${file}`);
}